import logo from './logo.svg';
import './App.css';
import {Card} from './carte';
import {Choix} from './carte';
import { useEffect, useState } from "react";



function App() {
  const [currentCardType, setCurrentCardType] = useState("");
  console.log(currentCardType);
  return (
    <div className="App">
      
       <h1>Hello Campus Init</h1>
      <h2>WE CAN</h2>
      <div class="bloc">
        <Card valeur="pique" setCurrentCardType={setCurrentCardType}></Card> 
        <Card valeur="trèfle" setCurrentCardType={setCurrentCardType}></Card>
        <Card valeur="coeur" setCurrentCardType={setCurrentCardType}></Card>
        <Card valeur="carreau" setCurrentCardType={setCurrentCardType}></Card> 
    </div>
   <Choix currentCardType={currentCardType}/>
    </div>
  );
}

export default App;
