import { useEffect, useState } from "react";

/*création cartes*/
export function Card(props) {
    const [ext, setExt] = useState("active");

    function handlClick(e) {
        if (ext === "active") {
            setExt("inactive");
            props.setCurrentCardType(props.valeur);
        }
        else {
            setExt("active");
        }
    }
    return (
        <div className={`Card ${ext}`} onClick={handlClick}>
            <div>
                <div class="recto">{props.valeur}</div>
                <div class="verso"></div>
            </div>
        </div>
    );
};

/*demande choix utilisateur*/
export function Choix(props) {
    const [couleur, setCouleur] = useState('');

    /*compare couleur carte*/
    function Compa(couleur) {
        console.log(props.currentCardType)
        if (couleur === props.currentCardType) {
            return <p>Tu es un {couleur}</p>
        }
        return <p>pas {couleur} rejoue</p>
    }


    function handleChangeCouleur(e) {
        setCouleur(e.target.value);
    }

    useEffect(() => {
        document.title = `La vie en  ${couleur}`;
    });

    return (
        <fieldset>
            <legend>choix de la couleur (coeur,pique,carreau,trèfle) : </legend>
            <input type="text" value={couleur} onChange={handleChangeCouleur} />
            {Compa(couleur)}
        </fieldset>
    );
}


